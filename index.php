<?php

$searchWords = ['php', 'html', 'internet', 'Web'];

$searchStrings = array(
	'Internet - большая сеть компьютеров, которые могут взаимодействовать друг с другом.',
	'PHP - это распространенный язык программирования с открытым исходным кодом.',
	'PHP сконструирован специально для ведения Web-разработок и его код может внедряться непосредственно в HTML'
);

foreach ($searchStrings as $index => $string) {
	if (preg_match_all('/Internet|php|web|html/i', $string, $matches)) {
		echo '<pre>';
		echo "In sentence " . ($index + 1) . " we have words:";
		print_r(implode(', ', $matches[0]));
		echo '</pre>';
	}
}